## Introduction 

Synergy module provides an option get list of entities that are referencing an entity. Each entity would get a separate tab to see this list.

## Design


| Board     | Individual     | Content item         | 
|-----------|----------------|----------------------|
| N/A       | List of boards | List of individuals  |

## Implementation

### Content item
<img width="1680" alt="synergy" src="https://cloud.githubusercontent.com/assets/1220029/23384408/bfe27be6-fd42-11e6-8329-f23bad47bb04.png">


### Individual component

<img width="1657" alt="synergy-individual" src="https://cloud.githubusercontent.com/assets/1220029/23384407/bfd109d8-fd42-11e6-8317-6e00e91128c7.png">


### Operation list
<img width="1657" alt="synergy-individual" src="http://g.recordit.co/ZPrXjwqP4E.gif">